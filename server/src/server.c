//
// Created by Brice Nourry-Cabrol on 5/21/20.
//
#include <sys/types.h>
#include "headers/server.h"
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>

int server(void) {
    printf("Creating Socket...");
    socket(AF_INET, SOCK_STREAM||SOCK_NONBLOCK, 0);
    puts(" \033[0;32m[Done]");
    endprotoent();
    return 0;
}